package ru.javajonnor.mymaps;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;


import org.osmdroid.config.Configuration;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;

public class MainActivity extends AppCompatActivity {

     MapView map;

     MapController mapController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        map = findViewById(R.id.mapsView);

        Configuration.getInstance().setUserAgentValue(getPackageName());


        mapController = (MapController) map.getController();
        mapController.setZoom(15);

        GeoPoint geoPoint = new GeoPoint(54.167421168225665, 45.13604766901434);
        mapController.setCenter(geoPoint);





    }
}